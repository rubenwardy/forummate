package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.Account
import com.rubenwardy.forummate.repositories.AccountRepository
import com.rubenwardy.forummate.utils.CookieProvider
import com.rubenwardy.forummate.utils.Status
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val accountRepository: AccountRepository,
    private val cookieProvider: CookieProvider
) : ViewModel() {
    var url: String = "https://google.com"
        set(value) {
            field = value

            notifyNewPage(field, cookieProvider.getCookie(field))
        }

    private val mutableLoading = MutableLiveData(true)
    private val mutableAccount = MediatorLiveData<Account>()

    val loading: LiveData<Boolean>
        get() = mutableLoading

    val account: LiveData<Account>
        get() = mutableAccount

    private fun notifyNewPage(url: String, cookies: Map<String, String>) {
        mutableLoading.value = false

        if (cookies.isEmpty()) {
            return
        }

        val newAccount = accountRepository.checkForLogin(url, cookies)
        mutableAccount.addSource(newAccount) {
            when (it.status) {
                Status.SUCCESS -> {
                    mutableLoading.value = false
                    it.data?.let { data -> mutableAccount.value = data }
                }
                Status.LOADING -> {
                    mutableLoading.value = true
                }
                Status.ERROR -> {}
            }
        }
    }
}
