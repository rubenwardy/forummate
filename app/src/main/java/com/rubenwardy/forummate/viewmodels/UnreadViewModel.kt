package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.Thread
import com.rubenwardy.forummate.repositories.ThreadRepository
import com.rubenwardy.forummate.utils.Result
import com.rubenwardy.forummate.utils.SingleLiveEvent
import javax.inject.Inject

class UnreadViewModel @Inject constructor(
    threadRepository: ThreadRepository
) : ViewModel() {
    val threads: LiveData<List<Thread>> = threadRepository.getUnreadThreads()
    val loading: LiveData<Result<Unit>> = threadRepository.updateUnreadThreads()

    private val mutableThreadClicked = SingleLiveEvent<Thread>()
    val threadClicked : LiveData<Thread?>
        get() = mutableThreadClicked

    fun onItemClick(item: Thread) {
        mutableThreadClicked.value = item
    }
}
