package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.ForumAccount
import com.rubenwardy.forummate.repositories.AccountRepository
import com.rubenwardy.forummate.repositories.ThreadRepository
import com.rubenwardy.forummate.utils.Result
import javax.inject.Inject

class PostCommentViewModel @Inject constructor(
    private val threadRepository: ThreadRepository,
    private val accountRepository: AccountRepository
) : ViewModel() {
    lateinit var account: LiveData<ForumAccount>
    var accountId: Long = 0
    var remoteId = ""

    val submitting = MediatorLiveData<Result<Unit>>()
    val canPost = MutableLiveData(false)

    var content = ""
        set(value) {
            field = value

            canPost.value = value.length > 3
        }

    fun post() {
        val result = threadRepository.postComment(accountId, remoteId, content)
        submitting.addSource(result) {
            submitting.value = it
        }
    }

    fun setup(accountId: Long, remoteId: String) {
        this.accountId = accountId
        this.remoteId = remoteId

        account = accountRepository.getAccount(accountId)
    }
}
