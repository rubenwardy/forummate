package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.Forum
import com.rubenwardy.forummate.repositories.ForumRepository
import com.rubenwardy.forummate.utils.SingleLiveEvent
import javax.inject.Inject

class FindForumViewModel @Inject constructor(
    forumRepository: ForumRepository
) : ViewModel() {
    val search = MutableLiveData<String>("")

    val loading = forumRepository.updateForumsFromAPI()

    private val allForums : LiveData<List<Forum>> = forumRepository.getForumList()
    val forums : LiveData<List<Forum>>
        get() = Transformations.switchMap(search) {
            Transformations.map(allForums) { forums ->
                if (!search.value.isNullOrEmpty()) {
                    forums.filter { forum ->
                        forum.title.contains(search.value!!, true)
                    }
                } else {
                    forums
                }
            }
        }

    private val mutableForumClicked = SingleLiveEvent<Forum>()
    val forumClicked : LiveData<Forum?>
        get() = mutableForumClicked

    fun onItemClick(forum: Forum) {
        mutableForumClicked.value = forum
    }
}
