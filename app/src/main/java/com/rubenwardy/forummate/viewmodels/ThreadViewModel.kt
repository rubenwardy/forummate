package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.Post
import com.rubenwardy.forummate.repositories.ThreadRepository
import com.rubenwardy.forummate.utils.Result
import org.jsoup.Jsoup
import javax.inject.Inject

class ThreadViewModel @Inject constructor(
    private val threadRepository: ThreadRepository
) : ViewModel() {

    var posts: LiveData<List<Post>>? = null
    var loading: LiveData<Result<Unit>>? = null
    var accountId: Long = 0
    var remoteId = ""

    fun setThreadId(accountId: Long, remoteId: String) {
        this.accountId = accountId
        this.remoteId = remoteId

        posts = threadRepository.getPosts(accountId, remoteId)
        loading = threadRepository.updatePosts(accountId, remoteId)
    }

    fun toText(content: String): String {
        return Jsoup.parse(content).text()
    }

}
