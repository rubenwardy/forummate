package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.ForumAccount
import com.rubenwardy.forummate.repositories.AccountRepository
import com.rubenwardy.forummate.utils.SingleLiveEvent
import javax.inject.Inject

class AccountViewModel @Inject constructor(
    private val accountRepository: AccountRepository
) : ViewModel() {

    var account : LiveData<ForumAccount>? = null
    var accountID : Long? = null
        set(v) {
            field = v
            v?.let { accountID ->
                account = accountRepository.getAccount(accountID)
            }
        }

    private val mutableNavigateUp = SingleLiveEvent<Boolean>()
    val navigateUp : LiveData<Boolean?>
        get() = mutableNavigateUp

    fun delete() {
        account?.value?.account?.let {
            accountRepository.deleteAccount(it)
        }

        mutableNavigateUp.value = true
    }
}
