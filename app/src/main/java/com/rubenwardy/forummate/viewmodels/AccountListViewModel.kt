package com.rubenwardy.forummate.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.rubenwardy.forummate.models.ForumAccount
import com.rubenwardy.forummate.repositories.AccountRepository
import com.rubenwardy.forummate.utils.SingleLiveEvent
import javax.inject.Inject

class AccountListViewModel @Inject constructor(
    accountRepository: AccountRepository
) : ViewModel() {

    val accounts : LiveData<List<ForumAccount>> = accountRepository.getAccounts()

    private val mutableAccountClicked = SingleLiveEvent<ForumAccount>()
    val accountClicked : LiveData<ForumAccount?>
        get() = mutableAccountClicked

    fun onItemClick(account: ForumAccount) {
        mutableAccountClicked.value = account
    }

}
