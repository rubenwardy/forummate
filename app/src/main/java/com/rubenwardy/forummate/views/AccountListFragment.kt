package com.rubenwardy.forummate.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentAccountsBinding
import com.rubenwardy.forummate.models.ForumAccount
import com.rubenwardy.forummate.viewmodels.AccountListViewModel
import com.rubenwardy.forummate.views.binding.BindingRecyclerAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class AccountListFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var accountListViewModel: AccountListViewModel

    private lateinit var binding: FragmentAccountsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)

        accountListViewModel =
            ViewModelProvider(this, viewModelFactory).get(AccountListViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_accounts, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accountListViewModel.accountClicked.observe(viewLifecycleOwner, Observer {
            it?.let {
                findNavController().navigate(AccountListFragmentDirections.actionViewAccount(it.account.id))
            }
        })

        binding.apply {
            accountList.apply {
                setHasFixedSize(true)
                adapter =
                    BindingRecyclerAdapter<ForumAccount, AccountListViewModel>(
                        accountListViewModel,
                        R.layout.item_account
                    )
                layoutManager = LinearLayoutManager(context)
            }

            fabAdd.setOnClickListener {
                findNavController().navigate(AccountListFragmentDirections.actionAddForum())
            }

            lifecycleOwner = this@AccountListFragment
            viewModel = accountListViewModel
        }
    }
}
