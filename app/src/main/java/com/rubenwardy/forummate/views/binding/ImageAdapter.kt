package com.rubenwardy.forummate.views.binding

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("image_url")
fun loadImage(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}

@BindingAdapter("src")
fun setImage(view: ImageView, bitmap: Bitmap?) {
    bitmap?.let {
        view.setImageBitmap(it)
    }
}
