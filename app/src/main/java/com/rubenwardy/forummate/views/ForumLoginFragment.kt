package com.rubenwardy.forummate.views

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentForumLoginBinding
import com.rubenwardy.forummate.viewmodels.LoginViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class ForumLoginFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding : FragmentForumLoginBinding

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)

        val cm = CookieManager.getInstance()
        cm.removeAllCookies(null)
        cm.flush()

        loginViewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        loginViewModel.url = arguments?.getString("url") ?: "https://google.com"

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forum_login, container, false)
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginViewModel.account.observe(viewLifecycleOwner) {
            findNavController().navigateUp()
        }

        binding.apply {
            webview.settings.javaScriptEnabled = true
            webview.settings.allowContentAccess = false
            webview.settings.allowFileAccess = false
            webview.settings.allowFileAccessFromFileURLs = false
            webview.settings.userAgentString =
                "com.rubenwardy.forummate/1 " + System.getProperty("http.agent")

            lifecycleOwner = this@ForumLoginFragment

            viewModel = loginViewModel
            invalidateAll()
        }
    }

}
