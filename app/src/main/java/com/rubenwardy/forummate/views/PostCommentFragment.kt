package com.rubenwardy.forummate.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentPostCommentBinding
import com.rubenwardy.forummate.viewmodels.PostCommentViewModel
import com.rubenwardy.forummate.viewmodels.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class PostCommentFragment : BottomSheetDialogFragment() {
    private val accountIdKey = "accountId"
    private val remoteIdKey = "remoteId"

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var postCommentViewModel: PostCommentViewModel
    private lateinit var binding: FragmentPostCommentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)

        postCommentViewModel = ViewModelProvider(this, viewModelFactory).get(PostCommentViewModel::class.java)
        postCommentViewModel.setup(arguments?.getLong(accountIdKey)!!, arguments?.getString(remoteIdKey)!!)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_comment, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = postCommentViewModel

            // Focus EditText and show keyboard
            editText.post(Runnable {
                editText.requestFocus()
                val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
            })
        }

        return binding.root
    }


}
