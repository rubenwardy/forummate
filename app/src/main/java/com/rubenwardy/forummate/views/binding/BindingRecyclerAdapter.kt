package com.rubenwardy.forummate.views.binding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.rubenwardy.forummate.BR


class BindingRecyclerAdapter<T, V>(val viewModel: V, @LayoutRes val layout: Int): RecyclerView.Adapter<BindingRecyclerAdapter.ViewHolder>() {
    var emptyView: View? = null
        set(v) {
            field = v
            updateEmptyView()
        }

    var data: List<T> = emptyList()
        set(v) {
            field = v
            notifyDataSetChanged()

            updateEmptyView()
        }

    private fun updateEmptyView() {
        emptyView?.visibility = if (data.isEmpty()) View.VISIBLE else View.GONE
    }

    class ViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(layoutInflater, layout, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.setVariable(BR.item, data.getOrNull(position))
        holder.binding.setVariable(BR.viewModel, viewModel)
    }

}


@BindingAdapter("data")
fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, data: List<T>?) {
    if (data == null) {
        return
    }

    if (recyclerView.adapter is BindingRecyclerAdapter<*, *>) {
        (recyclerView.adapter as BindingRecyclerAdapter<T, *>).data = data
    }
}


@BindingAdapter("empty_view")
fun setRecyclerViewProperties(recyclerView: RecyclerView, view: View) {
    if (recyclerView.adapter is BindingRecyclerAdapter<*, *>) {
        (recyclerView.adapter as BindingRecyclerAdapter<*, *>).emptyView = view
    }
}
