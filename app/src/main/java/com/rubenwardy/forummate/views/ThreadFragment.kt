package com.rubenwardy.forummate.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentThreadBinding
import com.rubenwardy.forummate.models.Post
import com.rubenwardy.forummate.viewmodels.ThreadViewModel
import com.rubenwardy.forummate.viewmodels.ViewModelFactory
import com.rubenwardy.forummate.views.binding.BindingRecyclerAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class ThreadFragment : Fragment() {
    private val accountIdKey = "accountId"
    private val remoteIdKey = "remoteId"

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var threadViewModel: ThreadViewModel
    private lateinit var binding: FragmentThreadBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)

        threadViewModel = ViewModelProvider(this, viewModelFactory).get(ThreadViewModel::class.java)
        threadViewModel.setThreadId(arguments?.getLong(accountIdKey)!!, arguments?.getString(remoteIdKey)!!)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_thread, container, false)
        binding.apply {
            postList.apply {
                setHasFixedSize(false)
                adapter =
                    BindingRecyclerAdapter<Post, ThreadViewModel>(
                        threadViewModel,
                        R.layout.item_post
                    )
                layoutManager = LinearLayoutManager(context)
            }

            fabReply.setOnClickListener {
                findNavController().navigate(
                    ThreadFragmentDirections.actionPostComment(threadViewModel.accountId,
                        threadViewModel.remoteId))
            }

            lifecycleOwner = viewLifecycleOwner
            viewModel = threadViewModel
        }
        return binding.root
    }

}
