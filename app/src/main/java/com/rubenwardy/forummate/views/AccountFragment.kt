package com.rubenwardy.forummate.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentAccountBinding
import com.rubenwardy.forummate.viewmodels.AccountViewModel
import com.rubenwardy.forummate.viewmodels.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class AccountFragment : Fragment() {
    private val accountIDKey = "accountId"

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var accountViewModel: AccountViewModel
    private lateinit var binding: FragmentAccountBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        accountViewModel = ViewModelProvider(this, viewModelFactory).get(AccountViewModel::class.java)
        accountViewModel.accountID = arguments?.getLong(accountIDKey)

        accountViewModel.navigateUp.observe(viewLifecycleOwner, Observer {
            findNavController().navigateUp()
        })

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = accountViewModel

            relogin.setOnClickListener {
                val url = accountViewModel.account?.value?.forum?.url!!
                findNavController().navigate(AccountFragmentDirections.actionLoginAgain(url))
            }
        }
        return binding.root
    }
}
