package com.rubenwardy.forummate.views

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.rubenwardy.forummate.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onSupportNavigateUp() =
        findNavController(R.id.mainNavigationFragment).navigateUp()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = findNavController(R.id.mainNavigationFragment)
        nav_view.setupWithNavController(navController)

        val config = AppBarConfiguration.Builder(nav_view.menu).build()
        setupActionBarWithNavController(navController, config)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            val visible = nav_view.menu.findItem(destination.id) != null
            nav_view.visibility = if (visible) { View.VISIBLE } else { View.GONE }
        }
    }
}
