package com.rubenwardy.forummate.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentUnreadBinding
import com.rubenwardy.forummate.models.Thread
import com.rubenwardy.forummate.utils.Status
import com.rubenwardy.forummate.views.binding.BindingRecyclerAdapter
import com.rubenwardy.forummate.viewmodels.UnreadViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class UnreadFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var unreadViewModel: UnreadViewModel
    private lateinit var binding: FragmentUnreadBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        unreadViewModel = ViewModelProvider(this, viewModelFactory).get(UnreadViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_unread, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        unreadViewModel.threadClicked.observe(viewLifecycleOwner, Observer {
            it?.let {
                findNavController().navigate(UnreadFragmentDirections.actionViewThread(it.accountId, it.remoteId))
            }
        })

        binding.apply {
            threadList.apply {
                setHasFixedSize(true)
                adapter =
                    BindingRecyclerAdapter<Thread, UnreadViewModel>(
                        unreadViewModel,
                        R.layout.item_thread
                    )
                layoutManager = LinearLayoutManager(context)
            }

            lifecycleOwner = this@UnreadFragment
            viewModel = unreadViewModel
        }

        unreadViewModel.loading.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {}
                Status.ERROR -> {
                    val msg = resources.getString(R.string.error_sync_failed, it.message)
                    Snackbar.make(binding.root, msg, Snackbar.LENGTH_LONG).show()
                }
                Status.LOADING -> {}
            }
        })
    }
}
