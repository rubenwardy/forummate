package com.rubenwardy.forummate.views.binding

import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener

@BindingAdapter("url")
fun setUrl(webview: WebView, url: String) = webview.loadUrl(url)

@InverseBindingAdapter(attribute="url")
fun getUrl(webview: WebView): String = webview.url

@BindingAdapter("app:urlAttrChanged")
fun setUrlListeners(
    webview: WebView,
    urlAttrChange: InverseBindingListener?
) {
    webview.webViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)

            urlAttrChange?.onChange()
        }
    }
}
