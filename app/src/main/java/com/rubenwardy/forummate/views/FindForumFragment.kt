package com.rubenwardy.forummate.views

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.SearchView.OnQueryTextListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenwardy.forummate.R
import com.rubenwardy.forummate.databinding.FragmentFindForumBinding
import com.rubenwardy.forummate.models.Forum
import com.rubenwardy.forummate.viewmodels.FindForumViewModel
import com.rubenwardy.forummate.views.binding.BindingRecyclerAdapter
import dagger.android.support.AndroidSupportInjection
import java.net.URLEncoder
import javax.inject.Inject

class FindForumFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var findForumViewModel: FindForumViewModel

    private lateinit var binding: FragmentFindForumBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        setHasOptionsMenu(true)

        findForumViewModel =
            ViewModelProvider(this, viewModelFactory).get(FindForumViewModel::class.java)

        findForumViewModel.forumClicked.observe(viewLifecycleOwner, Observer {
            it?.let {
                findNavController().navigate(FindForumFragmentDirections.actionLogin(it.login))
            }
        })

        // Hack to make the forum data load
        findForumViewModel.loading.observe(viewLifecycleOwner, Observer {})

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_find_forum, container, false)

        binding.apply {
            forumList.apply {
                setHasFixedSize(true)
                adapter =
                    BindingRecyclerAdapter<Forum, FindForumViewModel>(
                        findForumViewModel,
                        R.layout.item_forums
                    )
                layoutManager = LinearLayoutManager(context)
            }

            searchOnGoogle.setOnClickListener {
                val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS)

                val url = "http://google.com/search?q=" + URLEncoder.encode(findForumViewModel.search.value, "UTF-8")
                findNavController().navigate(FindForumFragmentDirections.actionLogin(url))
            }

            lifecycleOwner = this@FindForumFragment
            viewModel = findForumViewModel
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.search, menu)

        val search = menu.findItem(R.id.action_search).actionView as SearchView
        search.setOnQueryTextListener(object: OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(query: String?): Boolean {
                findForumViewModel.search.value = query
                return true
            }
        })

        search.setOnCloseListener {
            findForumViewModel.search.value = ""
            false
        }
    }
}
