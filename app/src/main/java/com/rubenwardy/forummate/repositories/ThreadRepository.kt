package com.rubenwardy.forummate.repositories

import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.rubenwardy.forummate.backends.ForumFactory
import com.rubenwardy.forummate.backends.phpbb.AuthenticationException
import com.rubenwardy.forummate.models.*
import com.rubenwardy.forummate.utils.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ThreadRepository @Inject constructor(
    private val threadDao: ThreadDao,
    private val postDao: PostDao,
    private val forumRepository: ForumRepository,
    private val accountRepository: AccountRepository,
    private val forumFactory: ForumFactory
) {
    @WorkerThread
    private fun insertThreadSync(thread: Thread) = threadDao.insert(thread)

    @WorkerThread
    private fun getThreadSync(accountId: Long, remoteId: String): ThreadWithAccount = threadDao.getByIdSync(accountId, remoteId)

    fun getUnreadThreads(): LiveData<List<Thread>> = threadDao.getAllUnread()

    @WorkerThread
    private fun insertPostSync(post: Post) = postDao.insert(post)

    fun getPosts(accountId: Long, threadRemoteId: String): LiveData<List<Post>> = postDao.getAllByThread(accountId, threadRemoteId)


    fun updateUnreadThreads(): LiveData<Result<Unit>> = liveData {
        emit(Result.loading<Unit>(null))

        withContext(Dispatchers.IO) {
            var error : String? = null

            for (account in accountRepository.getLoggedInAccountsSync()) {
                try {
                    forumFactory.getForum(account).getUnread().forEach {
                        insertThreadSync(it)
                    }
                } catch (e: AuthenticationException) {
                    // TODO: clear tokens
                    error = "forum account was logged out"
                } catch (e: IOException) {
                    error = e.message ?: e.toString()
                }
            }

            if (error == null) {
                emit(Result.success(null))
            } else {
                emit(Result.error(error, null))
            }
        }
    }


    fun updatePosts(accountId: Long, remoteId: String): LiveData<Result<Unit>> = liveData {
        emit(Result.loading<Unit>(null))

        withContext(Dispatchers.IO) {
            var error : String? = null

            try {
                val thread = getThreadSync(accountId, remoteId)
                val forumAccount = accountRepository.getAccountSync(thread.account.id)

                forumFactory.getForum(forumAccount).getPostsForThread(thread).forEach {
                    insertPostSync(it)
                }
            } catch (e: AuthenticationException) {
                // TODO: clear tokens
                error = "forum account was logged out"
            } catch (e: IOException) {
                error = e.message ?: e.toString()
            }

            if (error == null) {
                emit(Result.success(null))
            } else {
                emit(Result.error(error, null))
            }
        }
    }

    fun postComment(accountId: Long, remoteId: String, message: String): LiveData<Result<Unit>> = liveData {
        emit(Result.loading<Unit>(null))

        withContext(Dispatchers.IO) {
            var error : String? = null

            Log.e("Runnin", "Running!!!!")

            try {
                val thread = getThreadSync(accountId, remoteId)
                val forumAccount = accountRepository.getAccountSync(thread.account.id)

                forumFactory.getForum(forumAccount).postComment(thread, message)
            } catch (e: AuthenticationException) {
                // TODO: clear tokens
                error = "forum account was logged out"
            } catch (e: IOException) {
                error = e.message ?: e.toString()
            }

            if (error == null) {
                emit(Result.success(null))
            } else {
                emit(Result.error(error, null))
            }
        }
    }

}
