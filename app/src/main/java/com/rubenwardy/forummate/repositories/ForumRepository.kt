package com.rubenwardy.forummate.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.rubenwardy.forummate.api.ForumMateAPI
import com.rubenwardy.forummate.models.Forum
import com.rubenwardy.forummate.models.ForumDao
import com.rubenwardy.forummate.utils.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ForumRepository @Inject constructor(
    private val forumDao: ForumDao
) {
    fun getForum(forumId: Long): LiveData<Forum> = forumDao.getById(forumId)
    fun getForumByDomain(domain: String): LiveData<Forum> = forumDao.getByDomain(domain)
    fun getForumByDomainSync(domain: String): Forum? = forumDao.getByDomainSync(domain)
    fun getForumList(): LiveData<List<Forum>> = forumDao.getAll()
    fun insertForum(forum: Forum): Long = forumDao.insert(forum)
    fun updateForum(forum: Forum) = forumDao.update(forum)

    fun updateForumsFromAPI(): LiveData<Result<Unit>> = liveData {
        emit(Result.loading<Unit>(null))

        withContext(Dispatchers.IO) {
            var error : String? = null

            val api = ForumMateAPI.createService()
            try {
                val response = api.getForums().execute()
                if (response.isSuccessful && response.body() != null) {
                    val forums = response.body()!!.map {
                        Forum(it.title, it.url, it.login)
                    }
                    forumDao.insertAllIgnore(forums)
                } else {
                    error = "Unable to load forums"
                }
            } catch (ex: IOException) {
                error = ex.message
            }

            if (error == null) {
                emit(Result.success(null))
            } else {
                emit(Result.error(error, null))
            }
        }
    }

}
