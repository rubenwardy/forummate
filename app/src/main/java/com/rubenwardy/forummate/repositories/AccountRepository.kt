package com.rubenwardy.forummate.repositories

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.rubenwardy.forummate.backends.ForumFactory
import com.rubenwardy.forummate.models.Account
import com.rubenwardy.forummate.models.AccountDao
import com.rubenwardy.forummate.models.Forum
import com.rubenwardy.forummate.models.ForumAccount
import com.rubenwardy.forummate.utils.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccountRepository @Inject constructor(
        private val accountDao: AccountDao,
        private val forumRepository: ForumRepository,
        private val forumFactory: ForumFactory) {

    fun getAccount(accountID: Long): LiveData<ForumAccount> = accountDao.getById(accountID)

    @WorkerThread
    fun getAccountSync(accountID: Long): ForumAccount = accountDao.getByIdSync(accountID)

    @WorkerThread
    private fun getAccountByUsernameSync(username: String, forumID: Long): ForumAccount? = accountDao.getByUsernameSync(username, forumID)

    fun getAccounts(): LiveData<List<ForumAccount>> = accountDao.getAllWithForum()

    fun getLoggedInAccounts(): LiveData<List<ForumAccount>> = accountDao.getAllLoggedIn()

    @WorkerThread
    fun getLoggedInAccountsSync(): List<ForumAccount> = accountDao.getAllLoggedInSync()

    @WorkerThread
    private fun insertAccountSync(account: Account): Long = accountDao.insert(account)

    @WorkerThread
    private fun updateAccountSync(account: Account) = accountDao.updateSync(account)

    @WorkerThread
    fun deleteAccountSync(account: Account) = accountDao.delete(account)

    fun deleteAccount(account: Account) {
        CoroutineScope(Dispatchers.IO).launch {
            deleteAccountSync(account)
        }
    }

    fun checkForLogin(url: String, cookies: Map<String, String>) : LiveData<Result<Account>> = liveData {
        val strategy = forumFactory.getLoginStrategy(url, cookies) ?: run {
            emit(Result.success(null))
            return@liveData
        }

        emit(Result.loading(null))

        withContext(Dispatchers.IO) {
            val details = strategy.getAccessDetails(url, cookies) ?: run {
                emit(Result.success(null))
                return@withContext
            }

            val forumId = forumRepository.getForumByDomainSync(details.domain)?.id ?: run {
                forumRepository.insertForum(Forum(details.domain, "http;//${details.domain}", url))
            }

            var account = getAccountByUsernameSync(details.username, forumId)?.account
            if (account != null) {
                account.update(details.token)
                updateAccountSync(account)
            } else {
                account = Account(details.username, forumId, details.token)
                insertAccountSync(account)
            }
            emit(Result.success(account))
        }
    }
}
