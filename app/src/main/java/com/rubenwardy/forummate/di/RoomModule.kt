package com.rubenwardy.forummate.di

import com.rubenwardy.forummate.MyApplication
import com.rubenwardy.forummate.backends.ForumFactory
import com.rubenwardy.forummate.models.*
import com.rubenwardy.forummate.repositories.AccountRepository
import com.rubenwardy.forummate.repositories.ForumRepository
import com.rubenwardy.forummate.repositories.ThreadRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RoomModule {
    @Singleton
    @Provides
    internal fun providesRoomDatabase(appContext: MyApplication): AppDatabase
            = AppDatabase.getInstance(appContext)!!

    @Singleton
    @Provides
    internal fun providesAccountDao(appDatabase: AppDatabase): AccountDao
            = appDatabase.accountDao()

    @Singleton
    @Provides
    internal fun providesAccountRepository(accountDao: AccountDao, forumRepository: ForumRepository, forumFactory: ForumFactory): AccountRepository
            = AccountRepository(accountDao, forumRepository, forumFactory)

    @Singleton
    @Provides
    internal fun providesForumDao(appDatabase: AppDatabase): ForumDao
            = appDatabase.forumDao()

    @Singleton
    @Provides
    internal fun providesForumRepository(forumDao: ForumDao): ForumRepository
            = ForumRepository(forumDao)

    @Singleton
    @Provides
    internal fun providesThreadDao(appDatabase: AppDatabase): ThreadDao
            = appDatabase.threadDao()

    @Singleton
    @Provides
    internal fun providesPostDao(appDatabase: AppDatabase): PostDao
            = appDatabase.postDao()

    @Singleton
    @Provides
    internal fun providesThreadRepository(threadDao: ThreadDao,
                  postDao: PostDao,
                  forumRepository: ForumRepository,
                  accountRepository: AccountRepository,
                  forumFactory: ForumFactory): ThreadRepository
            = ThreadRepository(threadDao, postDao, forumRepository, accountRepository, forumFactory)
}
