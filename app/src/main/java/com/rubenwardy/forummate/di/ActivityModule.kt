package com.rubenwardy.forummate.di

import com.rubenwardy.forummate.views.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun bindMainActivity() : MainActivity
}
