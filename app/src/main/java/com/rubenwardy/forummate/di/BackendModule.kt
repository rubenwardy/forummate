package com.rubenwardy.forummate.di

import com.rubenwardy.forummate.backends.ForumFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BackendModule {
    @Singleton
    @Provides
    internal fun bind() : ForumFactory = ForumFactory()
}
