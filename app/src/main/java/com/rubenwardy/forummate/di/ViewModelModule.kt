package com.rubenwardy.forummate.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rubenwardy.forummate.viewmodels.*
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AccountListViewModel::class)
    internal abstract fun accountListViewModel(viewModel: AccountListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountViewModel::class)
    internal abstract fun accountViewModel(viewModel: AccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun accountLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FindForumViewModel::class)
    internal abstract fun findForumViewModel(viewModel: FindForumViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UnreadViewModel::class)
    internal abstract fun unreadViewModel(viewModel: UnreadViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ThreadViewModel::class)
    internal abstract fun threadViewModel(viewModel: ThreadViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostCommentViewModel::class)
    internal abstract fun postCommentViewModel(viewModel: PostCommentViewModel): ViewModel
}
