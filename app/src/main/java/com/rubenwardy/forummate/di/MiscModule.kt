package com.rubenwardy.forummate.di

import com.rubenwardy.forummate.utils.CookieManagerCookieProvider
import com.rubenwardy.forummate.utils.CookieProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MiscModule {
    @Singleton
    @Provides
    internal fun bind() : CookieProvider = CookieManagerCookieProvider()
}
