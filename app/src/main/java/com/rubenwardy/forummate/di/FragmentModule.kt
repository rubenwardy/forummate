package com.rubenwardy.forummate.di

import com.rubenwardy.forummate.views.*
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun bindAccountListFragment() : AccountListFragment

    @ContributesAndroidInjector
    abstract fun bindAccountFragment() : AccountFragment

    @ContributesAndroidInjector
    abstract fun bindForumLoginFragment() : ForumLoginFragment

    @ContributesAndroidInjector
    abstract fun bindFindForumFragment() : FindForumFragment

    @ContributesAndroidInjector
    abstract fun bindUnreadFragment() : UnreadFragment

    @ContributesAndroidInjector
    abstract fun bindThreadFragment() : ThreadFragment

    @ContributesAndroidInjector
    abstract fun bindPostCommentFragment() : PostCommentFragment
}
