package com.rubenwardy.forummate.utils

import android.webkit.CookieManager

class CookieManagerCookieProvider : CookieProvider {
    override fun getCookie(url: String) : Map<String, String> {
        val cookieString = CookieManager.getInstance().getCookie(url) ?: return mapOf()

        return cookieString
            .split(";")
            .map {
                val idx = it.indexOf("=")
                val key = it.substring(0, idx).trim()
                val value = it.substring(idx + 1).trim()
                key to value
            }
            .toMap()
    }
}
