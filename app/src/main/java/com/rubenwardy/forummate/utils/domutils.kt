package com.rubenwardy.forummate.utils

import android.annotation.SuppressLint
import org.jsoup.nodes.Element
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


private fun extractTextSegments2(results: MutableList<String>, element: Element) {
    element.childNodes().forEach {
        if (it is Element) {
            extractTextSegments2(results, it)
        } else {
            results.add(it.toString())
        }
    }
}

fun extractTextSegments(element: Element): List<String> {
    val results = mutableListOf<String>()
    extractTextSegments2(results, element)
    return results
}

@SuppressLint("SimpleDateFormat")
fun extractDate(element: Element): Date? {
    // 12 Feb 2020, 04:52
    val formats = listOf(
            SimpleDateFormat("EEE MMM dd yyyy hh:mm a"),
            SimpleDateFormat("MMM dd yy hh:mm a"),
            SimpleDateFormat("MMM dd yy HH:mm"),
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss"),
            SimpleDateFormat("dd MMM yyyy HH:mm"),
            SimpleDateFormat("yyyy/MM/dd HH:mm:ss"),
            SimpleDateFormat("dd MMMM yyyy"),
            SimpleDateFormat("yyyy-MM-dd"))

    for (segment in extractTextSegments(element)) {
        val clean = segment
            .replace(Regex("""[^A-Za-z0-9 \-:/]"""), "")
            .replace(Regex("""([0-9]+)[thsrd]{2}"""), "$1")
            .trim()
            .replace(Regex("in$"), "")
        if (clean.isEmpty()) {
            continue
        }

        for (format in formats) {
            try {
                val date = format.parse(clean)
                if (date != null) {
                    return date
                }
            } catch (ex: ParseException) {
                // pass
            }
        }

        for (regex in listOf(Regex("^[0-9]+$ minutes ago"))) {
            val match: MatchResult = regex.find(clean) ?: continue
            val value = match.groupValues[0].toLong()
            return Date(Date().time - value * 60)
        }
    }

    return null
}
