package com.rubenwardy.forummate.utils

interface CookieProvider {
    fun getCookie(url: String) : Map<String, String>
}
