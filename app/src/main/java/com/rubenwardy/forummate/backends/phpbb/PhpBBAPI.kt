package com.rubenwardy.forummate.backends.phpbb

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface PhpBBAPI {
    @GET("/")
    fun getHomePage(): Call<ResponseBody>

    @GET("search.php?search_id=unreadposts")
    fun getUnreadTopicList(): Call<ResponseBody>

    // @GET("feed.php")
    // fun getTopicPosts(@Query("t") topicId: Int): Call<List<PostListParser.Post>>

    @GET("viewtopic.php")
    fun getTopic(@Query("t") topicId: String): Call<ResponseBody>

    @POST
    @FormUrlEncoded
    fun postReply(@Url url: String, @FieldMap fields: Map<String, String>): Call<ResponseBody>
}
