package com.rubenwardy.forummate.backends.phpbb

import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.*

fun createRetrofit(domain: String, url: String, forumId: String, uId: String, sId: String, k: String): PhpBBAPI {
    val prefix = "phpbb$forumId"

    val useragent = "com.rubenwardy.forummate/1 " + System.getProperty("http.agent")

    val clientBuilder = OkHttpClient.Builder()
        .addNetworkInterceptor { chain ->
            chain.proceed(
                chain.request()
                    .newBuilder()
                    .header("User-Agent", useragent)
                    .build()
            )
        }
        .cookieJar(object: CookieJar {
            override fun loadForRequest(url: HttpUrl): MutableList<Cookie> {
                val uidCookie =
                    Cookie.Builder()
                        .name("${prefix}_u")
                        .expiresAt(Calendar.getInstance().time.time + 60*60*5)
                        .value(uId)
                        .domain(domain)
                        .path("/")
                        .build()

                val sidCookie =
                    Cookie.Builder()
                        .name("${prefix}_sid")
                        .expiresAt(Calendar.getInstance().time.time + 60*60*5)
                        .value(sId)
                        .domain(domain)
                        .path("/")
                        .build()

                val kCookie =
                    Cookie.Builder()
                        .name("${prefix}_k")
                        .expiresAt(Calendar.getInstance().time.time + 60*60*5)
                        .value(k)
                        .domain(domain)
                        .path("/")
                        .build()

                return mutableListOf(uidCookie, sidCookie, kCookie)
            }

        override fun saveFromResponse(url: HttpUrl, cookies: MutableList<Cookie>) {}
    })

    return Retrofit.Builder()
        .baseUrl(url)
        .client(clientBuilder.build())
        .build()
        .create(PhpBBAPI::class.java)
}
