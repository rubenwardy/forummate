package com.rubenwardy.forummate.backends

data class ForumAccessDetails(
    val domain: String,
    val username: String,
    val token: String)
