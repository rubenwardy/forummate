package com.rubenwardy.forummate.backends

import com.rubenwardy.forummate.models.Post
import com.rubenwardy.forummate.models.Thread
import com.rubenwardy.forummate.models.ThreadWithAccount
import java.io.IOException

interface ForumInterface {
    @Throws(IOException::class)
    fun getUnread(): List<Thread>

    @Throws(IOException::class)
    fun getUsername(): String?

    @Throws(IOException::class)
    fun getPostsForThread(thread: ThreadWithAccount): List<Post>

    @Throws(IOException::class)
    fun postComment(thread: ThreadWithAccount, message: String)
}
