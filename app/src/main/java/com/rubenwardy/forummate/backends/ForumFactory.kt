package com.rubenwardy.forummate.backends

import com.rubenwardy.forummate.backends.phpbb.PhpBBForum
import com.rubenwardy.forummate.backends.phpbb.PhpBBLoginStrategy
import com.rubenwardy.forummate.models.ForumAccount

class ForumFactory {
    fun getForum(account: ForumAccount): ForumInterface {
        return PhpBBForum(account)
    }

    fun getLoginStrategy(url: String, cookies: Map<String, String>): LoginStrategy? {
        val strategy = PhpBBLoginStrategy()

        return if (strategy.check(url, cookies)) {
            strategy
        } else {
            null
        }
    }
}
