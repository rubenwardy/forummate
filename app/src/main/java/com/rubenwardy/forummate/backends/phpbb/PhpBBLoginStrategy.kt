package com.rubenwardy.forummate.backends.phpbb

import com.rubenwardy.forummate.backends.ForumAccessDetails
import com.rubenwardy.forummate.backends.LoginStrategy
import com.rubenwardy.forummate.models.Account
import com.rubenwardy.forummate.models.Forum
import com.rubenwardy.forummate.models.ForumAccount
import java.net.URI

class PhpBBLoginStrategy : LoginStrategy {
    override fun check(url: String, cookies: Map<String, String>): Boolean {
        return getToken(cookies) != null
    }

    override fun getAccessDetails(url: String, cookies: Map<String, String>): ForumAccessDetails? {
        val token = getToken(cookies) ?: return null
        val domain = URI.create(url).host
        val username = getUsername(domain, token) ?: return null

        return ForumAccessDetails(domain, username, token)
    }

    private fun getToken(cookies: Map<String, String>): String? {
        val sidKey = cookies.keys.firstOrNull { it.startsWith("phpbb") && it.endsWith("_sid") }
            ?: return null

        val forumId = sidKey.substring(5, sidKey.length - 4)
        val sid = cookies[sidKey] ?: return null
        val userId = cookies["phpbb${forumId}_u"] ?: return null
        if (userId == "1") {
            return null
        }
        val k = cookies["phpbb${forumId}_k"] ?: return null

        return "$forumId:$userId:$sid:$k"
    }

    private fun getUsername(domain: String, token: String): String? {
        val forum = Forum("temporary", domain, "")
        val account = ForumAccount(Account("unknown", 0, token), listOf(forum))
        return PhpBBForum(account).getUsername()
    }
}
