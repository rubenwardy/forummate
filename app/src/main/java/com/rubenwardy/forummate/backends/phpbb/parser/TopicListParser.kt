package com.rubenwardy.forummate.backends.phpbb.parser

import com.rubenwardy.forummate.backends.phpbb.AuthenticationException
import com.rubenwardy.forummate.utils.extractDate
import org.jsoup.nodes.Element
import java.io.IOException
import java.util.*

class TopicListParser {

    data class Topic(val title: String, val url: String, val author: String, val lastUpdated: Date)

    @Throws(IOException::class)
    fun parse(root: Element) : List<Topic> {
        val rows = root
            .select(".topiclist.topics li.row")

        val title = root.select("title").text()

        if (rows.isEmpty() ||
                title.toLowerCase(Locale.getDefault()).contains("login")) {
            throw AuthenticationException("Login required")
        }

        return rows.map { parseTopicItem(it) }
    }

    private fun parseTopicItem(element: Element) : Topic {
        val link = element.select(".topictitle")
        val links = element.select("a")
        val author = links.first { it.attr("href").contains("memberlist") }.text()

        val lastUpdated = extractDate(element.select(".lastpost").single()) ?: Date()

        return Topic(link.text(), link.attr("href"), author, lastUpdated)
    }
}
