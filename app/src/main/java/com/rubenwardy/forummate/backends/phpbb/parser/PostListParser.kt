package com.rubenwardy.forummate.backends.phpbb.parser

import org.jsoup.nodes.Element

class PostListParser {

    data class Post(val title: String, val content: String, val author: String)
    data class SubmitTemplate(val url: String, val fields: Map<String, String>, val contentName: String)

    fun parse(root: Element) : List<Post> {
        return root
            .select(".post")
            .map { parsePostItem(it) }
    }

    private fun parsePostItem(element: Element) : Post {
        val author = element.select(".postbody .author a")
            .first { it.attr("href").contains("memberlist") }
            .text()
        val title = element.select(".postbody h3").single().text()
        val content = element.select(".postbody .content").single().html()

        return Post(title, content, author)
    }

    fun parseSubmitTemplate(root: Element): SubmitTemplate {
        val form = root.getElementById("qr_postform") ?: root.getElementById("postform")

        val subject = form.getElementById("subject").`val`()

        val fields = form.select("input[type='hidden']")
            .associateBy({ it.attr("name")  }, { it.`val`() })
            .toMutableMap()

        fields["subject"] = subject
        fields["post"] = "Submit"

        return SubmitTemplate(form.attr("action"), fields, "message")
    }
}
