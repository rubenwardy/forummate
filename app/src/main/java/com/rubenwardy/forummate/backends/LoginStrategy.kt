package com.rubenwardy.forummate.backends

/**
 * A LoginStrategy is a method of obtaining access details.
 *
 * The strategy pattern is used to choose between a number of approaches by asking them in term
 * whether the data is acceptable to them.
 */
interface LoginStrategy {

    /**
     * Checks if this is the correct strategy. Needs to be inexpensive to run on the main thread.
     *
     * @param url The URL.
     * @param cookies A map of cookie name to value.
     * @return boolean if this could be the right strategy
     */
    fun check(url: String, cookies: Map<String, String>): Boolean

    /**
     * Get the access details or return null if the request was not acceptable.
     *
     * @param url The URL.
     * @param cookies A map of cookie name to value.
     */
    fun getAccessDetails(url: String, cookies: Map<String, String>): ForumAccessDetails?

}
