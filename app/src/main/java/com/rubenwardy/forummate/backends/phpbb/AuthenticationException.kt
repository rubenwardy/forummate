package com.rubenwardy.forummate.backends.phpbb

class AuthenticationException(message: String) : Exception(message)
