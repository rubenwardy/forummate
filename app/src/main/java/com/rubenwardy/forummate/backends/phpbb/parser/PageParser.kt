package com.rubenwardy.forummate.backends.phpbb.parser

import org.jsoup.nodes.Element

class PageParser {
    data class Page(val username: String?)

    fun parse(root: Element): Page {
        return Page(findUsername(root))
    }

    private fun findUsername(root: Element): String? {
        val username = root.select("#username_logged_in .username-coloured").text().trim()
        if (username != "") {
            return username
        }

        val username2 = getUsername(root.select(".icon-logout a").text().trim())
        if (username2 != null && username2 != "") {
            return username2
        }

        return root
            .select(".linklist a")
            .mapNotNull { getUsername(it.text()) }
            .firstOrNull()
    }

    private fun getUsername(logout: String): String? {
        val start = logout.indexOf("[")
        val end = logout.indexOf("]")
        if (start >= end || start == -1 || end == -1) {
            return null
        }

        return logout.substring(start + 1, end).trim()
    }
}
