package com.rubenwardy.forummate.backends.phpbb

import android.net.Uri
import com.rubenwardy.forummate.BuildConfig
import com.rubenwardy.forummate.backends.ForumInterface
import com.rubenwardy.forummate.backends.phpbb.parser.PageParser
import com.rubenwardy.forummate.backends.phpbb.parser.PostListParser
import com.rubenwardy.forummate.backends.phpbb.parser.TopicListParser
import com.rubenwardy.forummate.models.ForumAccount
import com.rubenwardy.forummate.models.Post
import com.rubenwardy.forummate.models.Thread
import com.rubenwardy.forummate.models.ThreadWithAccount
import org.jsoup.Jsoup
import java.io.IOException

class PhpBBForum(private val account: ForumAccount) : ForumInterface {
    private val service: PhpBBAPI by lazy {
        val token = account.account.accessToken!!

        val parts = token.split(":")
        if (BuildConfig.DEBUG && parts.size != 4) {
            error("Assertion failed")
        }

        val forumId = parts[0]
        val uID = parts[1]
        val sID = parts[2]
        val k = parts[3]

        createRetrofit(account.forum.domain, account.forum.url, forumId, uID, sID,  k)
    }

    @Throws(IOException::class)
    override fun getUnread(): List<Thread> {
        val response = service.getUnreadTopicList().execute()
        if (!response.isSuccessful) {
            throw IOException(response.errorBody()?.toString())
        }

        val body = response.body()?.string() ?: throw IOException("Body was null")
        val document = Jsoup.parse(body)

        return TopicListParser().parse(document)
            .map {
                val remoteId = Uri.parse(it.url).getQueryParameter("t")!!
                Thread(account.account.id, remoteId, it.title, it.author, true)
            }
    }

    @Throws(IOException::class)
    override fun getUsername(): String? {
        val response = service.getHomePage().execute()
        if (!response.isSuccessful) {
            throw IOException(response.errorBody()?.toString())
        }

        val body = response.body()?.string() ?: throw IOException("Body was null")
        val document = Jsoup.parse(body)

        return PageParser().parse(document).username
    }

    @Throws(IOException::class)
    override fun getPostsForThread(thread: ThreadWithAccount): List<Post> {
        val response = service.getTopic(thread.thread.remoteId).execute()
        if (!response.isSuccessful) {
            throw IOException(response.errorBody()?.toString())
        }

        val body = response.body()?.string() ?: throw IOException("Body was null")
        return PostListParser().parse(Jsoup.parse(body))
            .map {
                Post(thread.thread.accountId, thread.thread.remoteId, it.title, it.author, it.content)
            }
    }

    @Throws(IOException::class)
    override fun postComment(thread: ThreadWithAccount, message: String) {
        // Get CSRF and other information
        val csrfResponse = service.getTopic(thread.thread.remoteId).execute()
        if (!csrfResponse.isSuccessful) {
            throw IOException(csrfResponse.errorBody()?.toString())
        }

        val body = csrfResponse.body()?.string() ?: throw IOException("Body was null")
        val submitTemplate = PostListParser().parseSubmitTemplate(Jsoup.parse(body))

        // Build request
        val fields = submitTemplate.fields.toMutableMap()
        fields[submitTemplate.contentName] = message

        // Post comment
        val postResponse = service.postReply(submitTemplate.url, fields).execute()
        if (!postResponse.isSuccessful) {
            throw IOException(postResponse.errorBody()?.toString())
        }
    }
}
