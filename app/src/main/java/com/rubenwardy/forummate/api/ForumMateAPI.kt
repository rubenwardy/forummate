package com.rubenwardy.forummate.api

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ForumMateAPI {
    @GET("forums.json")
    fun getForums(): Call<List<Forum>>

    class Forum {
        lateinit var title: String
        lateinit var url: String
        lateinit var login: String
    }

    companion object {
        fun createService(): ForumMateAPI {
            return Retrofit.Builder()
                .baseUrl("https://rubenwardy.com/forummate/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ForumMateAPI::class.java)
        }
    }
}
