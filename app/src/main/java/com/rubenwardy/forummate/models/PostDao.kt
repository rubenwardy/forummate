package com.rubenwardy.forummate.models

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(post: Post): Long

    @Query("SELECT * FROM post WHERE accountId = :accountId AND threadRemoteId = :threadRemoteId")
    fun getAllByThread(accountId: Long, threadRemoteId: String): LiveData<List<Post>>
}
