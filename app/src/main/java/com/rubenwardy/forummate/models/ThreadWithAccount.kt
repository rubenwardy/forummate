package com.rubenwardy.forummate.models

import androidx.room.Embedded
import androidx.room.Relation


class ThreadWithAccount(
        @Embedded
        val thread: Thread,
        @Relation(parentColumn="accountId", entityColumn="id")
        val account: Account)
