package com.rubenwardy.forummate.models

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface ThreadDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(thread: Thread): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg threads: Thread)

    @Query("SELECT * FROM thread WHERE accountId = :accountId AND remoteId = :remoteId")
    fun getById(accountId: Long, remoteId: String): LiveData<ThreadWithAccount>

    @Query("SELECT * FROM thread WHERE accountId = :accountId AND remoteId = :remoteId")
    fun getByIdSync(accountId: Long, remoteId: String): ThreadWithAccount

    @Query("SELECT * FROM thread")
    fun getAll(): LiveData<List<Thread>>

    @Query("SELECT * FROM thread WHERE unread = 1")
    fun getAllUnread(): LiveData<List<Thread>>
}
