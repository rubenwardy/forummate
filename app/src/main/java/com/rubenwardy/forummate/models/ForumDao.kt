package com.rubenwardy.forummate.models

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface ForumDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(forum: Forum): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg forums: Forum)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllIgnore(forums: List<Forum>)

    @Update
    fun update(forum: Forum)

    @Query("SELECT * FROM forum WHERE id = :forumID")
    fun getById(forumID: Long): LiveData<Forum>

    @Query("SELECT * FROM forum WHERE domain = :domain")
    fun getByDomain(domain: String): LiveData<Forum>

    @Query("SELECT * FROM forum WHERE domain = :domain")
    fun getByDomainSync(domain: String): Forum?

    @Query("SELECT * FROM forum ORDER BY title COLLATE NOCASE ASC")
    fun getAll(): LiveData<List<Forum>>
}
