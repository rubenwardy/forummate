package com.rubenwardy.forummate.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Post(
        var accountId: Long,
        var threadRemoteId: String,
        var title: String,
        var author: String,
        var content: String) {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

}
