package com.rubenwardy.forummate.models

import androidx.room.Embedded
import androidx.room.Relation


class ForumAccount(@Embedded val account: Account,
                   @Relation(parentColumn =  "forumId", entityColumn = "id")
                       private val _forum: List<Forum>) {
    val forum: Forum
        get() = _forum[0]
}
