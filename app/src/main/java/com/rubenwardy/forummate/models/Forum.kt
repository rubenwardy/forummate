package com.rubenwardy.forummate.models

import android.net.Uri
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices=[Index(value=["domain"], unique=true)])
data class Forum(var title: String, var url: String, var login: String,
                 var domain: String = Uri.parse(url).host ?: "?") {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    val iconUrl: String?
        get() = "$url/favicon.ico"
}
