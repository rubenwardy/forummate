package com.rubenwardy.forummate.models

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface AccountDao {
    @Insert(onConflict = REPLACE)
    fun insert(account: Account): Long

    @Insert(onConflict = REPLACE)
    fun insertAll(vararg accounts: Account)

    @Delete
    fun delete(account: Account)

    @Update
    fun updateSync(account: Account)

    @Query("SELECT * FROM account WHERE id = :accountID")
    fun getById(accountID: Long): LiveData<ForumAccount>

    @Query("SELECT * FROM account WHERE id = :accountID")
    fun getByIdSync(accountID: Long): ForumAccount

    @Query("SELECT * FROM account WHERE username = :username AND forumId = :forumID")
    fun getByUsernameSync(username: String, forumID: Long): ForumAccount?

    @Query("SELECT * FROM account")
    fun getAll(): LiveData<List<Account>>

    @Query("SELECT * FROM account WHERE forumId = :forumId")
    fun getAllByForum(forumId: Long): LiveData<List<Account>>

    @Query("SELECT * FROM account")
    fun getAllWithForum(): LiveData<List<ForumAccount>>

    @Query("SELECT * FROM account WHERE accessToken IS NOT NULL")
    fun getAllLoggedIn(): LiveData<List<ForumAccount>>

    @Query("SELECT * FROM account WHERE accessToken IS NOT NULL")
    fun getAllLoggedInSync(): List<ForumAccount>
}
