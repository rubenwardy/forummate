package com.rubenwardy.forummate.models

import androidx.room.Entity


@Entity(primaryKeys = ["accountId", "remoteId"])
data class Thread(
        var accountId: Long,
        var remoteId: String,
        var title: String,
        var author: String,
        var unread: Boolean = false)
