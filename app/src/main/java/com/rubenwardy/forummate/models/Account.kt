package com.rubenwardy.forummate.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(foreignKeys = [ ForeignKey(
            entity=Forum::class,
            parentColumns=["id"],
            childColumns=["forumId"],
            onDelete = ForeignKey.CASCADE) ])
data class Account(var username: String, var forumId: Long, var accessToken: String?) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    val expired: Boolean
        get() = accessToken == null

    fun update(token: String) {
        accessToken = token
    }
}
