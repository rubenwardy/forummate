package com.rubenwardy.forummate.backends.phpbb.parser

import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class PageParserTests(private val theme: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): Collection<String> = listOf("prosilver", "qsilver")
    }

    private val document: Element by lazy {
            val file = this::class.java.classLoader!!.getResource("backends/phpbb/parser/$theme/viewtopics.html")
            Jsoup.parse(file!!.readText())
        }


    @Test
    fun testPageParse() {
        val page = getTestedObject().parse(document)
        Assert.assertEquals("rubenwardy", page.username)
    }


    private fun getTestedObject(): PageParser {
        return PageParser()
    }

}
