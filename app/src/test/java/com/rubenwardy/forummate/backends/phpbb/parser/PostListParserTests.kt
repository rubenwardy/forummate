package com.rubenwardy.forummate.backends.phpbb.parser

import com.google.gson.Gson
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized


@RunWith(Parameterized::class)
class PostListParserTests(private val theme: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): Collection<String> = listOf("prosilver", "qsilver")
    }

    private val document: Element by lazy {
        val file = this::class.java.classLoader!!.getResource("backends/phpbb/parser/$theme/topic.html")
        Jsoup.parse(file!!.readText())
    }

    private val expected: List<PostListParser.Post> by lazy {
        val file = this::class.java.classLoader!!.getResource("backends/phpbb/parser/$theme/topic.json")
        Gson().fromJson(file!!.readText(), Array<PostListParser.Post>::class.java).asList()
    }

    @Test
    fun testParses() {
        val posts = getTestedObject().parse(document)

        // println(Gson().toJson(posts))

        Assert.assertEquals(expected.size, posts.size)
        expected.zip(posts).forEach { (expected, actual) ->
            Assert.assertEquals(expected, actual)
        }
    }

    private fun getTestedObject(): PostListParser {
        return PostListParser()
    }
}
