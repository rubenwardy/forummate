package com.rubenwardy.forummate.backends.phpbb.parser

import com.google.gson.Gson
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.*


@RunWith(Parameterized::class)
class TopicListParserTests(private val theme: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): Collection<String> = listOf("prosilver", "qsilver")
    }

    private val document: Element by lazy {
        val file = this::class.java.classLoader!!.getResource("backends/phpbb/parser/$theme/viewtopics.html")
        Jsoup.parse(file!!.readText())
    }

    private val expected: List<TopicListParser.Topic> by lazy {
        val file = this::class.java.classLoader!!.getResource("backends/phpbb/parser/$theme/viewtopics.json")
        Gson().fromJson(file!!.readText(), Array<TopicListParser.Topic>::class.java).asList()
    }


    @Test
    fun testTopicsMatch() {
        val topics = getTestedObject().parse(document)

        Assert.assertEquals(expected.size, topics.size)

        val relativeCutoff = Date(Date().time - 60*60)

        expected.zip(topics).forEach { (expected, actual) ->
            Assert.assertEquals(expected.author, actual.author)
            Assert.assertEquals(expected.title, actual.title)
            Assert.assertEquals(expected.url, actual.url)
            if (actual.lastUpdated < relativeCutoff) {
                Assert.assertEquals(expected.lastUpdated, actual.lastUpdated)
            }
        }
    }


    private fun getTestedObject(): TopicListParser {
        return TopicListParser()
    }

}
