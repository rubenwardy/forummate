package com.rubenwardy.forummate.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.rubenwardy.forummate.backends.ForumAccessDetails
import com.rubenwardy.forummate.backends.LoginStrategy
import com.rubenwardy.forummate.models.Account
import com.rubenwardy.forummate.repositories.AccountRepository
import com.rubenwardy.forummate.utils.CookieProvider
import com.rubenwardy.forummate.utils.Result
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class LoginViewModelTests {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var mockAccountRepo : AccountRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testDefaultUrl() {
        val viewModel = getTestedObject { emptyMap() }

        Assert.assertEquals("https://google.com", viewModel.url)
    }

    @Test
    fun testSetURLStopsLoading() {
        val viewModel = getTestedObject { emptyMap() }

        Assert.assertTrue(viewModel.loading.value ?: false)

        viewModel.url = "https://forum.minetest.net/"

        Assert.assertFalse(viewModel.loading.value ?: true)
    }

    @Test
    fun testSetURLWithNoCookiesDoesNothing() {
        val viewModel = getTestedObject { emptyMap() }

        Assert.assertEquals("https://google.com", viewModel.url)

        viewModel.url = "https://forum.minetest.net/"

        verify(mockAccountRepo, never()).checkForLogin(anyString(), any())
    }

    @Test
    fun testSetURLCallsCheckLogin() {
        val expectedURL = "https://forum.minetest.net/"
        val cookies = mapOf("cookieKey" to "cookieValue")

        val details = ForumAccessDetails("forum.minetest.net", "rubenwardy", "abcdef")

        val strategy = mock(LoginStrategy::class.java)
        `when`(strategy.getAccessDetails(expectedURL, cookies))
            .thenReturn(details)
        val mutableAccount = MutableLiveData(Result.success(Account("rubenwardy", 3, "abcdef")))
        `when`(mockAccountRepo.checkForLogin(anyString(), any()))
            .thenReturn(mutableAccount)

        val viewModel = getTestedObject {
            when (it) {
                expectedURL -> cookies
                else -> emptyMap()
            }
        }

        Assert.assertEquals("https://google.com", viewModel.url)

        runBlocking {
            viewModel.url = expectedURL
        }


        verify(mockAccountRepo).checkForLogin(viewModel.url, cookies)
    }

    private fun getTestedObject(cookieProvider: (String) -> Map<String, String>) : LoginViewModel {
        return LoginViewModel(mockAccountRepo, object : CookieProvider {
            override fun getCookie(url: String): Map<String, String> = cookieProvider(url)
        })
    }
}
