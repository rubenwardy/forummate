package com.rubenwardy.forummate.utils

import org.jsoup.Jsoup
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.text.SimpleDateFormat
import java.util.*


@RunWith(JUnit4::class)
class DomUtilsTests {

    @Test
    fun testGetsTextOfSingle() {
        val document = Jsoup.parse("<p>hello world this is me</p>")
        val expected = listOf("hello world this is me")

        Assert.assertEquals(expected, extractTextSegments(document))
    }

    @Test
    fun testGetsTextNested() {
        val document = Jsoup.parse("<p>hello <b>world</b> this is me</p>")
        val expected = listOf("hello ", "world", " this is me")

        Assert.assertEquals(expected, extractTextSegments(document))
    }

    @Test
    fun testGetsTextSiblings() {
        val document = Jsoup.parse("<p><b>hello world</b><i>this is me</i></p>")
        val expected = listOf("hello world", "this is me")

        Assert.assertEquals(expected, extractTextSegments(document))
    }

    @Test
    fun testGetLinkStuff() {
        val document = Jsoup.parse(
            "<span>\nby <a href=\"./memberlist.php?mode=viewprofile&amp;u=19832\">sorcerykid</a>\n" +
            "\t\t\t\t\t\t<br><a href=\"./viewtopic.php?f=3&amp;t=8499&amp;p=367556#p367556\">Tue Feb 18, 2020 1:52 am</a><br> </span>"
        )
        val expected = listOf("by", "sorcerykid", "", "Tue Feb 18, 2020 1:52 am", "")

        Assert.assertEquals(expected, extractTextSegments(document).map { it.trim() })
    }

    @Test
    fun testDateParsing() {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val test: (String, String) -> Unit = { expected, str ->
            Assert.assertEquals(format.parse(expected), extractDate(Jsoup.parse(str)))
        }

        test("2020-01-24 00:00", "24th January 2020")
        test("2020-01-24 22:01", "2020/01/24 22:01:00")
        test("2020-02-08 17:10", "» 08 Feb 2020, 17:10")
        test("2020-06-18 6:28", "Thu Jun 18, 2020 6:28 am")
        test("2020-01-24 00:00", "2020-01-24")
        test("2020-06-19 18:42", "19 Jun 2020, 18:42")
        test("2020-06-19 18:42", "19 Jun 2020 18:42")
        test("2020-06-19 18:42", "Jun 19th, '20, 18:42")
        test("2020-06-19 18:42", "June 19th, 2020, 6:42 pm")
        // test("2020-01-24 00:00", "50 minutes ago")
        // test("2020-01-24 00:00", "Today, 09:12")
    }
}
